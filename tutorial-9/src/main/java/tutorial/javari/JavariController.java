package tutorial.javari;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import tutorial.javari.animal.Animal;
import tutorial.javari.animal.AnimalDao;
import tutorial.javari.animal.AnimalRequest;


@RestController
public class JavariController {

    @RequestMapping(value = "/javari", method = RequestMethod.GET)
    @ResponseBody
    public Object getAnimals() {
        Map<String, Object> response = new HashMap<>();

        List<Animal> animals = AnimalDao.INSTANCE.getAnimals();
        response.put("animals", animals);

        if (animals.isEmpty()) {
            response.put("warning", "There are no animals here.");
        }

        return response;
    }

    @RequestMapping(value = "/javari", method = RequestMethod.POST)
    @ResponseBody
    public Object addAnimal(@RequestBody AnimalRequest animalRequest) {

        Animal animal = animalRequest.toAnimal();
        animal = AnimalDao.INSTANCE.persistAnimal(animal);

        return animal;
    }

    @RequestMapping(value = "/javari/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Object getAnimalFromId(@PathVariable(value = "id") int id) {
        Optional<Animal> animal = AnimalDao.INSTANCE.getAnimalFromId(id);
        if (!animal.isPresent()) {
            Map<String, Object> response = new HashMap<>();
            response.put("warning", "Animal not found.");
            return response;
        }

        return animal.get();
    }

    @RequestMapping(value = "/javari/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Object removeAnimalFromId(@PathVariable(value = "id") int id) {
        Optional<Animal> animal = AnimalDao.INSTANCE.getAnimalFromId(id);
        if (!animal.isPresent()) {
            Map<String, Object> response = new HashMap<>();
            response.put("warning", "Animal not found.");
            return response;
        }

        AnimalDao.INSTANCE.deleteAnimalFromId(id);

        return animal.get();
    }
}
