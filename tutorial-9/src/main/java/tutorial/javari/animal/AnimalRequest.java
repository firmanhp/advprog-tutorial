package tutorial.javari.animal;

/**
 * This class represents a JSON request for adding animals.
 *
 * @author firmanhp
 */

public class AnimalRequest {

    private String type;
    private String name;
    private Double length;
    private Double weight;
    private Gender gender;
    private Condition condition;

    public AnimalRequest() {
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void setGender(String gender) {
        this.gender = Gender.parseGender(gender);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCondition(String condition) {
        this.condition = Condition.parseCondition(condition);

    }

    public Animal toAnimal() {

        if (type == null
                || name == null
                || length == null
                || weight == null
                || gender == null
                || condition == null) {
            throw new IllegalArgumentException("Type, name, length, "
                    + "weight, gender, condition must not be empty");
        }

        return new Animal(null, type, name, gender, length,
                weight, condition);
    }
}
