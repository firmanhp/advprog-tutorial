package tutorial.javari.animal;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;


public enum AnimalDao {

    INSTANCE();

    private String dataLocation = "animals_records.csv";
    private final String[] headers = {
        "id",
        "type",
        "name",
        "gender",
        "length",
        "weight",
        "condition"
    };

    private Map<Integer, Animal> animals;

    AnimalDao() {
        loadCsv(dataLocation);
    }

    public Optional<Animal> getAnimalFromId(int id) {
        if (animals.containsKey(id)) {
            return Optional.of(animals.get(id));
        }
        return Optional.empty();
    }

    public void deleteAnimalFromId(int id) {
        if (!animals.containsKey(id)) {
            throw new IllegalArgumentException("Animal not found.");
        }

        animals.remove(id);
        this.updateDatabase();
    }

    public Animal persistAnimal(Animal animal) {
        if (animal == null) {
            throw new IllegalArgumentException("Animal must not be null");
        }

        Integer id = animal.getId();
        String type = animal.getType();
        String name = animal.getName();
        Gender gender = animal.getGender();
        double length = animal.getLength();
        double weight = animal.getWeight();
        Condition condition = animal.getCondition();

        if (id == null) {
            id = 1;
            while (animals.containsKey(id)) {
                id++;
            }
        }

        Animal animalToSave = new Animal(
                id,
                type,
                name,
                gender,
                length,
                weight,
                condition
        );
        animals.put(id, animalToSave);
        this.updateDatabase();

        return animalToSave;
    }

    public List<Animal> getAnimals() {
        return new ArrayList<>(animals.values());
    }

    private void updateDatabase() {
        writeCsv(dataLocation);
    }

    private synchronized void loadCsv(String csvPath) {
        animals = new HashMap<>();

        try (Reader in = new FileReader(csvPath)) {
            Iterable<CSVRecord> records = CSVFormat.DEFAULT
                    .withHeader(headers)
                    .withFirstRecordAsHeader()
                    .parse(in);

            for (CSVRecord record : records) {
                animals.put(Integer.parseInt(record.get("id")),
                        new Animal(
                                Integer.parseInt(record.get("id")),
                                record.get("type"),
                                record.get("name"),
                                Gender.parseGender(record.get("gender")),
                                Double.parseDouble(record.get("length")),
                                Double.parseDouble(record.get("weight")),
                                Condition.parseCondition(record.get("condition"))
                        ));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private synchronized void writeCsv(String csvPath) {
        try (Writer out = new FileWriter(csvPath)) {
            CSVPrinter printer = new CSVPrinter(out, CSVFormat.DEFAULT.withRecordSeparator("\n"));

            printer.printRecord(Arrays.asList(headers));

            for (Integer id : animals.keySet()) {
                Animal animal = animals.get(id);

                printer.printRecord(Arrays.asList(
                        animal.getId(),
                        animal.getType(),
                        animal.getName(),
                        animal.getGender().toString(),
                        animal.getLength(),
                        animal.getWeight(),
                        animal.getCondition().toString()
                ));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * A test only method, do not call.
     * @param testCsvPath CSV test file to load.
     */
    public void provideTestEnvironment(String testCsvPath) {
        dataLocation = testCsvPath;
        loadCsv(testCsvPath);
    }
}
