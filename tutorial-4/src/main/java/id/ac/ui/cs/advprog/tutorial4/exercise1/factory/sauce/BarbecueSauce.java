package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

public class BarbecueSauce implements Sauce {

    public String toString() {
        return "Barbecue Sauce";
    }
}
