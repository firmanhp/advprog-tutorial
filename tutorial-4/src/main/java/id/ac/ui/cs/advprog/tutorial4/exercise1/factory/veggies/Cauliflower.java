package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

public class Cauliflower implements Veggies {

    public String toString() {
        return "Cauliflower";
    }
}
