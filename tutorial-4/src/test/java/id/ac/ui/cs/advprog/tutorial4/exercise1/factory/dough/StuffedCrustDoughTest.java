package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class StuffedCrustDoughTest {
    private StuffedCrustDough stuffedCrustDough;

    @Before
    public void setUp() {
        stuffedCrustDough = new StuffedCrustDough();
    }

    @Test
    public void testToString() {
        assertEquals("Stuffed Crust Dough", stuffedCrustDough.toString());
    }
}
