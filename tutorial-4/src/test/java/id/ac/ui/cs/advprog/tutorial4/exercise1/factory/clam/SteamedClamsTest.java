package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class SteamedClamsTest {
    private SteamedClams steamedClams;

    @Before
    public void setUp() {
        steamedClams = new SteamedClams();
    }

    @Test
    public void testToString() {
        assertEquals("Steamed Clams", steamedClams.toString());
    }
}
