package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class CasuMarzuCheeseTest {
    private CasuMarzuCheese casuMarzuCheese;

    @Before
    public void setUp() {
        casuMarzuCheese = new CasuMarzuCheese();
    }

    @Test
    public void testToString() {
        assertEquals("Casu Marzu Cheese", casuMarzuCheese.toString());
    }
}
