package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class BarbecueSauceTest {
    private BarbecueSauce barbecueSauce;

    @Before
    public void setUp() {
        barbecueSauce = new BarbecueSauce();
    }

    @Test
    public void testToString() {
        assertEquals("Barbecue Sauce", barbecueSauce.toString());
    }
}
