package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class CauliflowerTest {
    private Cauliflower cauliflower;

    @Before
    public void setUp() {
        cauliflower = new Cauliflower();
    }

    @Test
    public void testToString() {
        assertEquals("Cauliflower", cauliflower.toString());
    }
}
