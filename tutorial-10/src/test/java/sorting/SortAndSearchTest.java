package sorting;

import static sorting.Finder.slowSearch;
import static sorting.Sorter.fastSort;
import static sorting.Sorter.slowSort;

import org.junit.Assert;
import org.junit.Test;

public class SortAndSearchTest {

    @Test
    public void testSlowSort() {
        int[] testArray = {12, 5, 9, 2, 2, 12, 3};
        int[] expectedArray = {2, 2, 3, 5, 9, 12, 12};
        testArray = slowSort(testArray);

        Assert.assertArrayEquals(testArray, expectedArray);
    }

    @Test
    public void testFastSort() {
        int[] testArray = {12, 5, 9, 2, 2, 12, 3};
        int[] expectedArray = {2, 2, 3, 5, 9, 12, 12};
        testArray = fastSort(testArray);

        Assert.assertArrayEquals(testArray, expectedArray);
    }

    @Test
    public void testSlowSearchFound() {
        int[] testArray = {12, 5, 9, 2, 2, 12, 3};

        Assert.assertEquals(slowSearch(testArray, 2), 2);
    }

    @Test
    public void testSlowSearchNotFound() {
        int[] testArray = {12, 5, 9, 2, 2, 12, 3};

        Assert.assertEquals(slowSearch(testArray, 4), -1);
    }
}
