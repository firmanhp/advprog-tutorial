package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

import java.util.Arrays;
import java.util.List;

public class MainClass {

    public static void main(String[] args) {

        Food thickBunBurgerSpecial = BreadProducer.THICK_BUN.createBreadToBeFilled();
        List<FillingDecorator> ingredientThickBunBurgerSpecial = Arrays.asList(
                FillingDecorator.BEEF_MEAT,
                FillingDecorator.CHEESE,
                FillingDecorator.CUCUMBER,
                FillingDecorator.LETTUCE,
                FillingDecorator.CHILI_SAUCE
        );

        for (FillingDecorator fillingDecorator : ingredientThickBunBurgerSpecial) {
            thickBunBurgerSpecial = fillingDecorator.addFillingToBread(thickBunBurgerSpecial);
        }

        System.out.println(thickBunBurgerSpecial.getDescription()
                + ", costs " + thickBunBurgerSpecial.cost());
    }
}
