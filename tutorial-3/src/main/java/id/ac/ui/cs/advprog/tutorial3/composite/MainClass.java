package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.NetworkExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.SecurityExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.UiUxDesigner;

import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

public class MainClass {

    public static void main(String[] args) {
        Company company = new Company();

        List<Supplier<Employees>> employeesSupplier;
        employeesSupplier = Arrays.asList(
            () -> {
                return new Ceo("Luffy", 500000.00); },
            () -> {
                return new Cto("Zorro", 320000.00); },
            () -> {
                return new BackendProgrammer("Franky", 94000.00); },
            () -> {
                return new BackendProgrammer("Usopp", 200000.00); },
            () -> {
                return new FrontendProgrammer("Nami", 66000.00); },
            () -> {
                return new FrontendProgrammer("Robin", 130000.00); },
            () -> {
                return new UiUxDesigner("sanji", 177000.00); },
            () -> {
                return new NetworkExpert("Brook", 83000.00); },
            () -> {
                return new SecurityExpert("Chopper", 90000.00); },
            () -> {
                return new SecurityExpert("Chopper", 10000.00); }
        );

        for (Supplier<Employees> supplier : employeesSupplier) {
            try {
                company.addEmployee(supplier.get());
            } catch (IllegalArgumentException e) {
                System.out.println(e.getMessage());
            }
        }

        System.out.println("Net salary: " + company.getNetSalaries());
    }

}
