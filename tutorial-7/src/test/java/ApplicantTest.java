import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import applicant.Applicant;

import java.util.function.Predicate;

import org.junit.Test;

public class ApplicantTest {

    private static final Applicant applicant = new Applicant();

    @Test
    public void testIsCredible() {
        assertTrue(applicant.isCredible());
    }

    @Test
    public void testGetCreditScore() {
        assertEquals(700, applicant.getCreditScore());
    }

    @Test
    public void testGetEmploymentYears() {
        assertEquals(10, applicant.getEmploymentYears());
    }

    @Test
    public void testHasCriminalRecord() {
        assertTrue(applicant.hasCriminalRecord());
    }

    @Test
    public void testEvaluateTrue() {
        Predicate<Applicant> applicantPredicate = applicant -> true;
        assertTrue(Applicant.evaluate(applicant, applicantPredicate));
    }

    @Test
    public void testEvaluateFalse() {
        Predicate<Applicant> applicantPredicate = applicant -> false;
        assertFalse(Applicant.evaluate(applicant, applicantPredicate));
    }

}
