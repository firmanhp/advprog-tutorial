import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class ScoreGroupingTest {

    private static final Map<String, Integer> TEST_SCORES = new HashMap<>();

    @Before
    public void setUp() {
        TEST_SCORES.put("Alice", 12);
        TEST_SCORES.put("Bob", 15);
        TEST_SCORES.put("Charlie", 11);
        TEST_SCORES.put("Delta", 15);
        TEST_SCORES.put("Emi", 15);
        TEST_SCORES.put("Foxtrot", 11);
    }

    @Test
    public void testGroupByScores() {
        Map<Integer, List<String>> expected = new HashMap<>();
        expected.put(11, Arrays.asList("Charlie", "Foxtrot"));
        expected.put(12, Arrays.asList("Alice"));
        expected.put(15, Arrays.asList("Bob", "Delta", "Emi"));

        Map<Integer, List<String>> actual = ScoreGrouping.groupByScores(TEST_SCORES);

        // Sort to make sure same values are in order
        for (Map.Entry<Integer, List<String>> entry : actual.entrySet()) {
            Collections.sort(entry.getValue());
        }

        assertEquals(expected, actual);
    }
}