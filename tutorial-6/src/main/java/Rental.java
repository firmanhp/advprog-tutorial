import java.util.List;

class Rental {

    private Movie movie;
    private int daysRented;

    public Rental(Movie movie, int daysRented) {
        this.movie = movie;
        this.daysRented = daysRented;
    }

    public Movie getMovie() {
        return movie;
    }

    public int getDaysRented() {
        return daysRented;
    }

    public double calculateAmount() {
        double amount = 0;

        switch (this.movie.getPriceCode()) {
            case Movie.REGULAR:
                amount += 2;
                if (this.daysRented > 2) {
                    amount += (this.daysRented - 2) * 1.5;
                }
                break;
            case Movie.NEW_RELEASE:
                amount += this.daysRented * 3;
                break;
            case Movie.CHILDREN:
                amount += 1.5;
                if (this.daysRented > 3) {
                    amount += (this.daysRented - 3) * 1.5;
                }
                break;
            default:
        }

        return amount;
    }

    public static int getFrequentRenterPoints(List<Rental> rentals) {
        // Initial value
        int frequentRenterPoints = rentals.size();

        for (Rental rental : rentals) {
            // Add bonus for a two day new release rental
            if ((rental.getMovie().getPriceCode() == Movie.NEW_RELEASE)
                    && rental.getDaysRented() > 1) {
                frequentRenterPoints++;
            }
        }

        return frequentRenterPoints;
    }
}