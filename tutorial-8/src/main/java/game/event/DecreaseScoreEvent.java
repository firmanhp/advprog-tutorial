package game.event;

import game.Score;

public class DecreaseScoreEvent implements Event {

    private Score score;
    private int decreaseValue;

    public DecreaseScoreEvent(Score score, int value) {
        this.score = score;
        this.decreaseValue = value;
    }

    public void run() {
        score.add(-decreaseValue);
    }

}
