package game.event;

public class EventIntervalWrapper {

    private Event event;
    private int intervalInSeconds;

    public EventIntervalWrapper(Event event, int intervalInSeconds) {
        this.event = event;
        this.intervalInSeconds = intervalInSeconds;
    }

    public Event getEvent() {
        return this.event;
    }

    public int getIntervalInSeconds() {
        return intervalInSeconds;
    }

}
