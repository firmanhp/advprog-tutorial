package game.event;

public interface Event {

    void run();

}
